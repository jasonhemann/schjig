;; -*- coding: utf-8; mode: scheme -*-

;; Copyright (C) 2013 Göran Weinholt <goran@weinholt.se>

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

;; Generate test cases for Scheme operators.

;; The test cases are not guaranteed to be free of false positives
;; (especially if inexacts or complex numbers are enabled).

;; Example invocation, comparing Chez and Guile:
;; (echo '(import (rnrs))';petite --script schjig.scm)|tee input.txt|guile|tee output.txt

(import (rnrs)
        (rnrs eval))

;; Change these to your liking.
(define *use-examples* #t)
(define *use-inexact* #t)
(define *use-complex* #f)
(define *unicode-limit* #x110000) ;#x10000 for IronScheme, otherwise #x110000
(define *fixnum-width* 61)        ;adjust to the target implementation
(define *max-optargs* 10)
(define *tests-per-op* 100)
(define *ignore-exceptions* #t)  ;disable to find missing error checks

(define /dev/urandom
  (open-file-input-port "/dev/urandom"
                        (file-options)
                        (buffer-mode none)))

;; z     complex number object
;; x     real number object
;; y     real number object
;; q     rational number object
;; n     integer object
;; k     exact non-negative integer object
;; bool  boolean (#f or #t)
;; char  character
;; ei    exact integer object
;; fx    fixnum
;; fl    flonum
;; ifl   integer flonum

;; For a bit index or an exponent:
;; ei^   exact integer object in [0,4096]
;; z^    complex number object, limited in size

(define ops
  '#(((bool) number? obj)
     ((bool) complex? obj)
     ((bool) real? obj)
     ((bool) rational? obj)
     ((bool) integer? obj)
     ((bool) real-valued? obj)
     ((bool) rational-valued? obj)
     ((bool) integer-valued? obj)
     ((bool) exact? z)
     ((bool) inexact? z)
     ((z) exact z)
     ((bool) = z z z ...)
     ((bool) < x x x ...)
     ((bool) > x x x ...)
     ((bool) <= x x x ...)
     ((bool) >= x x x ...)
     ((bool) zero? z)
     ((bool) positive? x)
     ((bool) negative? x)
     ((bool) odd? n)
     ((bool) even? n)
     ((bool) finite? x)
     ((bool) infinite? x)
     ((bool) nan? x)
     ((x) max x x ...)
     ((x) min x x ...)
     ((z) + z ...)
     ((z) * z ...)
     ((z) - z z ...)
     ((z) / z z ...)
     ((z) abs x)
     ((x x) div-and-mod x x)
     ((x) div x x)
     ((x) mod x x)
     ((x x) div0-and-mod0 x x)
     ((x) div0 x x)
     ((x) mod0 x x)
     ((n) gcd n ...)
     ((n) lcm n ...)
     ((n) numerator q)
     ((n) denominator q)
     ((n) floor x)
     ((n) ceiling x)
     ((n) truncate x)
     ((n) round x)
     ((x) rationalize x x)
     ((n n) exact-integer-sqrt k)
     ((bool) not obj)
     ((bool) boolean? obj)
     ((bool) boolean=? bool bool bool ...)
     ((bool) char? obj)
     ((sv) char->integer char)
     ((char) integer->char sv)
     ;; r6rs-lib
     ((char) char-upcase char)
     ((char) char-downcase char)
     ((char) char-titlecase char)
     ((char) char-foldcase char)
     ((bool) char=? char char char ...)
     ((bool) char<? char char char ...)
     ((bool) char>? char char char ...)
     ((bool) char<=? char char char ...)
     ((bool) char>=? char char char ...)
     ((bool) char-alphabetic? char)
     ((bool) char-numeric? char)
     ((bool) char-whitespace? char)
     ((bool) char-upper-case? char)
     ((bool) char-lower-case? char)
     ((bool) char-title-case? char)
     ((category) char-general-category char)
     ((ei) bitwise-not ei)
     ((ei) bitwise-and ei ...)
     ((ei) bitwise-ior ei ...)
     ((ei) bitwise-xor ei ...)
     ((ei) bitwise-if ei ei ei)
     ((ei) bitwise-bit-count ei)
     ((ei) bitwise-length ei)
     ((ei) bitwise-first-bit-set ei)
     ((bool) bitwise-bit-set? ei ei^)
     ((ei) bitwise-copy-bit ei ei^ ei^)
     ((ei) bitwise-bit-field ei ei^ ei^)
     ((ei) bitwise-copy-bit-field ei ei^ ei^ ei^)
     ((ei) bitwise-arithmetic-shift ei ei^)
     ((ei) bitwise-arithmetic-shift-left ei ei^)
     ((ei) bitwise-arithmetic-shift-right ei k)
     ((ei) bitwise-rotate-bit-field ei ei^ ei^ ei^)
     ((ei) bitwise-reverse-bit-field ei ei^ ei^)
     ((bool) fixnum? obj)
     ((fx) fixnum-width)
     ((fx) least-fixnum)
     ((fx) greatest-fixnum)
     ((bool) fx=? fx fx fx ...)
     ((bool) fx<? fx fx fx ...)
     ((bool) fx>? fx fx fx ...)
     ((bool) fx<=? fx fx fx ...)
     ((bool) fx>=? fx fx fx ...)
     ((bool) fxzero? fx)
     ((bool) fxpositive? fx)
     ((bool) fxnegative? fx)
     ((bool) fxodd? fx)
     ((bool) fxeven? fx)
     ((fx) fxmax fx fx ...)
     ((fx) fxmin fx fx ...)
     ((fx) fx+ fx fx)
     ((fx) fx* fx fx)
     ((fx) fx- fx fx)
     ((fx) fx- fx)
     ((fx fx) fxdiv-and-mod fx fx)
     ((fx) fxdiv fx fx)
     ((fx) fxmod fx fx)
     ((fx fx) fxdiv0-and-mod0 fx fx)
     ((fx) fxdiv0 fx fx)
     ((fx) fxmod0 fx fx)
     ;; Nobody uses anything but the reference implementations of these.
     ;; ((fx fx) fx+/carry fx fx fx)
     ;; ((fx fx) fx-/carry fx fx fx)
     ;; ((fx fx) fx*/carry fx fx fx)
     ((fx) fxnot fx)
     ((fx) fxand fx ...)
     ((fx) fxior fx ...)
     ((fx) fxxor fx ...)
     ((fx) fxif fx fx fx)
     ((fx) fxbit-count fx)
     ((fx) fxlength fx)
     ((fx) fxfirst-bit-set fx)
     ((bool) fxbit-set? fx fx)
     ((fx) fxcopy-bit fx fx fx)
     ((fx) fxbit-field fx fx fx)
     ((fx) fxcopy-bit-field fx fx fx fx)
     ((fx) fxarithmetic-shift fx fx)
     ((fx) fxarithmetic-shift-left fx fx)
     ((fx) fxarithmetic-shift-right fx fx)
     ((fx) fxrotate-bit-field fx fx fx fx)
     ((fx) fxreverse-bit-field fx fx fx)
     ((bool) eq? obj obj)
     ((bool) eqv? obj obj)
     ((bool) equal? obj obj)))

(define inexact-ops
  '#(((z) inexact z)
     ((z) exp z)
     ((z) log z)
     ((z) log z z)
     ((z) sin z)
     ((z) cos z)
     ((z) tan z)
     ((z) asin z)
     ((z) acos z)
     ((z) atan z)
     ((z) atan z z)
     ((z) expt z^ z^)
     ;; r6rs-lib
     ((bool) flonum? obj)
     ((fl) real->flonum x)
     ((bool) fl=? fl fl fl ...)
     ((bool) fl<? fl fl fl ...)
     ((bool) fl>? fl fl fl ...)
     ((bool) fl<=? fl fl fl ...)
     ((bool) fl>=? fl fl fl ...)
     ((bool) flinteger? fl)
     ((bool) flzero? fl)
     ((bool) flpositive? fl)
     ((bool) flnegative? fl)
     ((bool) flodd? ifl)
     ((bool) fleven? ifl)
     ((bool) flfinite? fl)
     ((bool) flinfinite? fl)
     ((bool) flnan? fl)
     ((fl) flmax fl fl ...)
     ((fl) flmin fl fl ...)
     ((fl) fl+ fl ...)
     ((fl) fl* fl ...)
     ((fl) fl- fl ...)
     ((fl) fl/ fl ...)
     ((fl) flabs fl)
     ;; ((fl fl) fldiv-and-mod fl fl)
     ;; ((fl) fldiv fl fl)
     ;; ((fl) flmod fl fl)
     ;; ((fl fl) fldiv0-and-mod0 fl fl)
     ;; ((fl) fldiv0 fl fl)
     ;; ((fl) flmod0 fl fl)
     ((fl) flnumerator fl)
     ((fl) fldenominator fl)
     ((fl) flfloor fl)
     ((fl) flceiling fl)
     ((fl) fltruncate fl)
     ((fl) flround fl)
     ((fl) flexp fl)
     ((fl) fllog fl)
     ((fl) fllog fl fl)
     ((fl) flsin fl)
     ((fl) flcos fl)
     ((fl) fltan fl)
     ((fl) flasin fl)
     ((fl) flacos fl)
     ((fl) flatan fl)
     ((fl) flatan fl fl)
     ((fl) flsqrt fl)
     ((fl) flexpt fl fl)
     ((fl) fixnum->flonum fx)))

(define complex-ops
  '#(((z) sqrt z)
     ((z) make-rectangular x x)
     ((z) make-polar x x)
     ((x) real-part z)
     ((x) imag-part z)
     ((x) magnitude z)
     ((x) angle z)))

(define *least-fixnum*
  (- (expt 2 (- *fixnum-width* 1))))

(define *greatest-fixnum*
  (- (expt 2 (- *fixnum-width* 1)) 1))

(define (op-args-type* op)
  (define (make-list n k)
    (vector->list (make-vector n k)))
  ;; Expand the ... in an op's argument list.
  (let ((t* (cddr op)))
    (if (and (pair? t*) (eq? (last t*) '...))
        (let ((t* (drop-last t*)))
          (append (drop-last t*)
                  (make-list (random (+ 1 *max-optargs*))
                             (last t*))))
        t*)))

(define (random n)
  (define (make-random-bytevector n)
    (get-bytevector-n /dev/urandom n))
  ;; XXX: NOT FOR CRYPTO USE. This stuff is just here so that SRFI-27
  ;; will not be needed.
  (let ((m (div (+ (+ 7 8) (bitwise-length n)) 8)))
    (let ((bv (make-random-bytevector m)))
      (mod (bytevector-uint-ref bv 0 (endianness big) (bytevector-length bv))
           n))))

(define (random-bool)
  (eqv? (random 2) 0))

(define (random-int n)
  (- (random (- (* 2 n) 1)) (- n 1)))

(define (print . x) (for-each display x) (newline))

(define (last x*)
  (if (null? (cdr x*))
      (car x*)
      (last (cdr x*))))

(define (drop-last x*)
  (if (null? (cdr x*))
      '()
      (cons (car x*)
            (drop-last (cdr x*)))))

(define *examples*
  (make-eq-hashtable))

(define *previous*
  (make-eq-hashtable))

(define (save-example type code)
  (when *use-examples*
    (hashtable-update! *examples* type
                       (lambda (ht)
                         (let ((ht (or ht (make-eqv-hashtable))))
                           (hashtable-set! ht code #f)
                           ht))
                       #f))
  (hashtable-set! *previous* type code))

(define-syntax maybe-quote
  (lambda (x)
    (syntax-case x ()
      ((_ op arg* ...)
       (with-syntax (((t* ...) (generate-temporaries #'(arg* ...))))
         #'(let ((t* arg*) ...)
             (if (exists list? (list t* ...))
                 (list 'op t* ...)
                 (op t* ...))))))))

(define (maybe-inexact x)
  (if (and *use-inexact* (zero? (random 20)))
      (maybe-quote inexact x)
      x))

(define (gen type)
  (cond ((and *use-examples*
              (zero? (random 10))
              (hashtable-ref *examples* type #f))
         => (lambda (ht)
              ;; Find an example expression of the requested type.
              (let ((k (hashtable-keys ht)))
                (vector-ref k (random (vector-length k))))))
        ((and (zero? (random 20))
              (hashtable-ref *previous* type #F))
         => (lambda (prev)
              ;; Reuse the previously generated value, or one close to it.
              (let ((v (case type
                         ((z x q n k)
                          (if (zero? (random 5))
                              (maybe-quote * prev (random-int 9))
                              (maybe-quote + prev (random-int 4))))
                         (else prev))))
                (case type
                  ((k) (maybe-quote abs v))
                  (else v)))))
        (else
         (case type
           ((z)
            (maybe-inexact
             (case (random 6)
               ((0) (gen 'x))
               ((1) (gen 'q))
               ((2) (gen 'n))
               ((3)
                (if *use-complex*
                    (maybe-quote make-rectangular (gen 'x) (gen 'x))
                    (gen 'x)))
               ((4)
                (if *use-complex*
                    (maybe-quote make-polar (gen 'x) (gen 'x))
                    (gen 'x)))
               (else (gen 'k)))))
           ((x)
            (if *use-inexact*
                (case (random 10)
                  ((0) +nan.0)
                  ((1) +inf.0)
                  ((2) -inf.0)
                  (else (gen 'q)))
                (gen 'q)))
           ((q)
            (maybe-inexact
             (case (random 5)
               ((0)
                (let ((den (gen 'n)))
                  (if (eqv? den 0)
                      (gen 'n)
                      (maybe-quote / (gen 'n) den))))
               ((1)
                (/ (+ 1 (random 1000))
                   (+ 1 (random 1000))))
               (else
                (gen 'n)))))
           ((k)
            ;; Be biased towards numbers that trigger tricky cases.
            (case (random 20)
              ((0) (random 4))
              ((1) (+ (expt 2 61) (random-int 4)))
              ((2) (+ (expt 2 60) (random-int 4)))
              ((3) (+ (expt 2 31) (random-int 4)))
              ((4) (+ (expt 2 30) (random-int 4)))
              ((5) (random (expt 2 16)))
              ((6) (random (expt 2 31)))
              ((7) (+ (expt 2 (+ 2 (random 64))) (random-int 4)))
              (else (random (* *greatest-fixnum* 2)))))
           ((ei)
            (if (zero? (random 2))
                (maybe-quote - (gen 'k))
                (gen 'k)))
           ((n)
            (maybe-inexact (gen 'ei)))
           ((ei^)
            ;; Used for exponents, bit indices, etc.
            (if (zero? (random 2))
                (maybe-quote - (random 4097))
                (random 4097)))
           ((z^)
            ;; Complex number. Limited so as to not cause an
            ;; out-of-memory error.
            (let retry ()
              (define (large? n)
                (or (> (abs (numerator n)) 512)
                    (> (abs (denominator n)) 512)))
              (let ((n (gen 'z)))
                (if (or (not (number? n))
                        (and (exact? n)
                             (or (large? (real-part n))
                                 (large? (imag-part n)))))
                    (gen 'ei^)
                    n))))
           ((bool)
            (random-bool))
           ((sv)
            (maybe-quote char->integer (gen 'char)))
           ((char)
            (integer->char
             (case (random 3)
               ((0) (random (min #x100 *unicode-limit*)))
               ((1) (random (min #xD800 *unicode-limit*)))
               (else
                (if (< *unicode-limit* #xE000)
                    (random *unicode-limit*)
                    (+ #xE000 (random (- *unicode-limit* #xE000))))))))
           ((fx)
            (case (random 20)
              ((0) *least-fixnum*)
              ((1) *greatest-fixnum*)
              ((2) (random-int 4))
              (else (+ (* (random-int (expt 2 4))
                          (expt 2 (random (- *fixnum-width* 4))))
                       (random-int 8)))))
           ((fl)
            (maybe-quote inexact (gen 'x)))
           ((ifl)
            (maybe-quote inexact (gen 'ei)))
           ((obj)
            ;; A little bit of anything goes.
            (case (random 3)
              ((0) (gen 'bool))
              ((1) (gen 'char))
              (else (gen 'z))))
           (else
            (error 'gen "Can't generate an object of this type" type))))))

(define env
  (environment '(rnrs)))

(define *old-tests*
  (guard (exn
          ((error? exn)
           ;; Ikarus never implemented equal-hash.
           #f))
    (equal-hash '(foo))
    (make-hashtable equal-hash equal?)))

(define *ntests* 0)

(define (write-test lhs rhs)
  (set! *ntests* (+ *ntests* 1))
  (write `(check ,lhs => ',rhs))
  (newline))

(define (gen-new-test name arg-type*)
  ;; XXX: this does not absolutely avoid duplicate tests, since
  ;; arg-type* may be too restrictive.
  (let lp ((i 0))
    (let ((code `(,name ,@(map gen arg-type*))))
      (cond ((and (< i 10) *old-tests* (hashtable-ref *old-tests* code #f))
             (lp (+ i 1)))
            (else
             (when *old-tests*
               (hashtable-set! *old-tests* code #t))
             code)))))

(define (emit-op-tests op)
  (do ((i 0 (+ i 1)))
      ((= i *tests-per-op*))
    (let retry ((attempts 0))
      (unless (> attempts 10)
        (let ((ret-type* (car op))
              (name (cadr op))
              (arg-type* (op-args-type* op)))
          ;; (print ";; Fuzzing " name " with args " arg-type*)
          (let ((code (gen-new-test name arg-type*)))
            ;; (display ";; ") (write code) (newline)
            (for-each save-example
                      arg-type* (cdr code))
            (guard (exn
                    (*ignore-exceptions*
                     (retry (+ attempts 1)))
                    (else
                     (write-test `(guard (exn (else 'exception))
                                    ,code)
                                 'exception)
                     (newline)))
              (let-values ((ret* (eval `(letrec ((fixnum-width
                                                  (lambda () ,*fixnum-width*))
                                                 (greatest-fixnum
                                                  (lambda () ,*greatest-fixnum*))
                                                 (least-fixnum
                                                  (lambda () ,*least-fixnum*)))
                                          ,code)
                                       env)))
                (cond
                  ((exists
                    (lambda (ret-type ret)
                      (case ret-type
                        ((fx) (not (<= *least-fixnum* ret *greatest-fixnum*)))
                        ((char) (not (<= (char->integer ret) *unicode-limit*)))
                        (else #f)))
                    ret-type* ret*)
                   ;; The result wouldn't be a fixnum or char in the
                   ;; target Scheme.
                   (retry (+ attempts 1)))
                  ((= (length ret*) 1)
                   (write-test code (car ret*))
                   (save-example (car ret-type*) code))
                  (else
                   (write-test `(let-values ((x ,code)) x)
                               ret*)))
                (for-each save-example
                          ret-type* ret*)))))))))



;; Print a program.

(print `(import #;(srfi :78 lightweight-testing)
                (rnrs)))

;; Simplified SRFI-78
(write
 `(begin
    (define *correct* 0)
    (define *failed* 0)

    (define (do-check expr p e equal?)
      (write expr)
      (display "\n=> \n")
      (let ((v (p)))
        (write v)
        (cond ((equal? v e)
               (set! *correct* (+ *correct* 1))
               (display "\n; correct\n\n"))
              (else
               (set! *failed* (+ *failed* 1))
               (display "\n; *** failed ***\n; expected result: ")
               (write e)
               (display "\n\n")))))

    (define (check-report)
      (display "; *** checks *** : ")
      (display *correct*)
      (display " correct, ")
      (display *failed*)
      (display " failed.\n"))

    (define-syntax check
      (syntax-rules (=>)
        ((_ expr => expect)
         (do-check 'expr (lambda () expr) expect equal?))))))
(newline)

(vector-for-each emit-op-tests ops)
(when *use-inexact*
  (vector-for-each emit-op-tests inexact-ops))
(when *use-complex*
  (vector-for-each emit-op-tests complex-ops))

(print `(check-report))

(write `(begin
          (display "; There might be as many as ")
          (display ,*ntests*)
          (display " correct tests.\n")))
(newline)
